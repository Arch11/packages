#!/bin/bash
arch=x86_64
cd ../pkgbuild
git submodule init && git submodule update
for d in ./*/ ; do (cd "$d" && makepkg -sf && mv *.pkg.tar.zst ../../packages/$arch); done
